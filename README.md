# NAME:

Profiler

# OVERVIEW:

Profiler was written in shadow of profiler from recon-ng. It is a reinvention of the wheel, so to speak. However, it does come with additonal features. New sites are being added and maintained. The objective of profiler is to hastily parse websites for exisiting usernames, without using API keys. This feature may come later. 

Usernames are considered found based off of the returned status codes given from web servers. This can lead to false positives, but rare. Most web servers return accurate results, than not. 

Profiler can also be used to set the timeout of the website request, as well as, set different user agents, write profile findings to a file, allow or dis-allow redirects of websites, and automatically open the found URL's of users.

# PREREQUESITES:

Python 3.6 and higher will be needed since the code includes f strings. The following modules are also needed, argparse, requests and webbrowser. Pre packaged modules include sys, os, platform, and subprocess.

# INSTALLATION:

Python 3.6 can be downloaded from https://www.python.org/

The modules argparse, requests, and webbrowser can be installed using PIP. The other modules should come pre-packeged with Python.

# OPERATING INSTRUCTIONS:

-u, --user = set the username to search.

-t, --timeout = set the request timeout.

-a, --agent = set a different user agent.
		List of user agents include chrome, firefox, and safarai.

-r, --allow-redirects = allow redirections of websites.
		May lead to false positives, but can also lead to more thorough searches.

-o, --open-pages = Prompts default webbrowser to lead url sites of found user.

-l, --list-agents = List current user agents.

-w, --write = Write findings to file.
		Files are written in current path on Windows.
		Files are written in /etc/profiler/ on Linux/OSX.
		Files are written as profiler.[user]

-v, --version = display version information.

# EXAMPLE USAGE:

profiler.py -h = help menu

profiler.py -u, --user [username]

profiler.py -u, --user [username] -t, --timeout [timeout] = set timeout for requests

profiler.py -u, --user [username] -a, --agent chrome = set chrome user agent

profiler.py -u, --user [username] -r, --allow-redirects = allow redirects

profiler.py -u, --user [username] -o, --open-pages = open found url's in web broswer

profiler.py -l, --list-agents = list user agents available 

profiler.py -u, --user [username] -w, --write = write to file. 
		Files are written in current directory on Windows. 
		Files are written to /etc/profiler/ on Linux and OSX. Directory will be created if it does not exist.

profiler.py -v, --version = display version information

# LICENSE:

Free to use, modify, distribute to whomever.

# AUTHOR:

Written and coded by Matthew Greer

# BUGS:

Please report bugs or errors to DFC302@protonmail.com




